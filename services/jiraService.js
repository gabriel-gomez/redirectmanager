/**
 * Created by Gabe on 6/8/17.
 */

var jiraApi = require('jira').JiraApi;
var config = require('../config/config-dev');
var jira = new jiraApi('https', config.jira.host, config.jira.port, config.jira.username, config.jira.password, '2', true);
var exec = require('child_process').execSync;
var logger = require('./loggerService');

/**
 * This retrieves the start date, end date, source url and destination url that is associated with JIRA issue
 *
 * @param issueId - JIRA ticket key
 * @param callback - Return error or response
 */
var findIssue = function(issueId, callback) {

        jira.findIssue(issueId, function(error, issue) {
            if (error) {
                logger.error("Error in findIssue function. Following error:\n",error);
                callback(error);
                return;
            }

            var body = {issueId: issue.id,
                        startDate: issue.fields[config.jira.customfields.startDate],
                        endDate: issue.fields[config.jira.customfields.endDate],
                        redirectFrom: issue.fields[config.jira.customfields.redirectFrom],
                        redirectTo: issue.fields[config.jira.customfields.redirectTo]};
            callback(null, body);

        // console.log('Status: ' + JSON.stringify(issue, null, 1));
    });
};

/**
 * This transitions a JIRA issue based of its key and transition ID passed
 *
 * @param issueId - JIRA ticket key
 * @param transitionId - Transition ID that will need to take place for JIRA ticket
 */
var issueTransition = function(issueId, transitionId, callback) {
    logger.debug('Starting the issueTransiton function for issue ID:', issueId);
    var id = {
        "transition": {
            "id": config.jira.transitions[transitionId]
        }
    };

        jira.transitionIssue(issueId, id, function(error, issue) {
            if (error) {
                logger.error("Error in issueTransition function. Following error:\n", error);
                callback(error);
                return;
            }
            logger.debug('The issueTransition function has fully executed with a status: ' + issue);
    });

};

/**
 * This is a function that checks if the source URL given on the JIRA ticket has been used in another source URL for a
 * ticket that is already live.
 *
 * @param issueId - The issue ID of the ticket that has been put into the Push status
 * @param sourceUrl - The source URL that is found in the "Redirect From" label for the issue ID in question
 * @param callback - returns the response (for now)
 */
var searchJira = function(issueId, sourceUrl, callback) {
    logger.debug('Starting the searchJira function');
    var options = {
        startAt: 0,
        maxResults: 50,
        fields: [config.jira.customfields.redirectFrom]
    };

    var response = null; // Instantiating in order to capture response once we call jira.searchJira

    // The JQL used in order to see if source URL has already been used
    var jql = 'project = "Redirect Manager" and status = Live and "Redirect From" ~ "' + sourceUrl + '"';

    jira.searchJira(jql, options, function(error, res) {
        if (error) {
            logger.error("In searchJira function. Following error:\n", error);
            callback(error);
            return;
        }

        response = res;
        logger.debug('searchJira function has fully executed and now doing the callback');
        callback(null, res);
    });
};

/**
 * This function determines whether or not an issue should be rejected to open or implemented to live based on the
 * response total received from the searchJira function results.
 *
 * @param issueId - Issue ID of the ticket being checked
 * @param issueKey - Issue key of the ticket being checked
 * @param responseTotal - The response total recieved from searchJira function
 * @param sourceUrl - the URL that can be found in the redirect from field of the ticket
 * @param finalDestination - The URL that can be found in the redirect to field of the ticket
 * @param callback - The error or response given
 */
var determiner = function(issueId, issueKey, responseTotal, sourceUrl, finalDestination, callback) {
    logger.debug('Starting the determiner function');

    // Checking to see if Tickets with source URL already exist in "Redirect From" field
    if (responseTotal > 0) {

        var comment = 'The source URL within the redirect from label has already been used in a previous ticket ' +
                'that is currently live. The issue key of the previous use is ' + issueKey;

        jira.addComment(issueId, comment, function(err) {
            if (err) {
                logger.error("In the determiner function. Following error:\n", err);
                callback(err);
                return;
            }
        });

        logger.debug('A previous ticket that is currently live with the same source URL has been found. Rejecting');
        issueTransition(issueId, "rejected");
    }

    else {
        var domain = 'https://www.citrix.com';

        if (sourceUrl.indexOf(domain) === -1) {
            sourceUrl = domain + sourceUrl;
        }

        if (finalDestination.indexOf(domain) === -1) {
            finalDestination = domain + finalDestination;
        }

        var results = exec("curl -ILs -m 20 -w '%{http_code}_%{url_effective} ' -o /dev/null " + finalDestination, {encoding: 'utf8'});
        var split = results.split('_');

        if (split[1] === finalDestination) {
            logger.debug('Final destination of provided source URL matches final destination provided. Going Live.');
            issueTransition(issueId, "implemented");
        }

        else {
            logger.debug('Final destination provided does not match final destination the source URL goes to. Rejecting');

            var comment = "It seems that the actual final destination of the source URL is not the one entered." +
                " The final destination received is " + split[1];

            jira.addComment(issueId, comment, function(err) {
                if (err) {
                    logger.error("In determiner function. Following error:\n" + err);
                    callback(err);
                }
               return;
            });

            issueTransition(issueId, "rejected");
            callback(null, split[1]);
        }
    }
};

var textUpdate = function(sourceUrl, finalDestination) {
    return;
};

module.exports = {findIssue: findIssue, issueTransition: issueTransition, searchJira: searchJira, determiner: determiner};